import React from "react";
import Button from "react-bootstrap/Button";

const handleClick = () => {
  const apiUrl = "https://jsonplaceholder.typicode.com/todos/5";
  fetch(apiUrl)
    .then((response) => response.json())
    .then((result) => {
      console.log(result);
    });
};

function Dashboard() {
  return (
    <div className="">
      <Button variant="primary" onClick={handleClick}>
        Submit
      </Button>
    </div>
  );
}

export default Dashboard;
